import { TextField } from '@mui/material';
import React, { useState } from 'react';
import './index.css'
import { useFormik } from 'formik';
import { confirmSchema } from '../../utils/validation';

const ConfirmPage = () => {
  const [name, setName] = useState('')


  const initial = {
    name:'',
    country:'',
    email: '',
    phone: '',
    companyName: '',
};
const { handleChange, handleSubmit, handleBlur, values, errors, touched } = useFormik({
    initialValues: initial,
    validationSchema: confirmSchema,
    onSubmit: () => submitMemberShip(),
});


  return (
    <div className="confirm_body">
      <div className='confirm_form_field'>
        <TextField
          sx={{ width: '100%' }}
          name='name'
          type='text'
          label="name"
          value={name}
          onChange={(event) => {
            setName(event.target.value);
          }}
        />
        <TextField
          sx={{ width: '100%' }}
          name='country'
          type='text'
          label="Country"
          value={name}
          onChange={(event) => {
            setName(event.target.value);
          }}
        />
        <TextField
          sx={{ width: '100%' }}
          name='email'
          type='email'
          label="Email"
          value={name}
          onChange={(event) => {
            setName(event.target.value);
          }}
        />
        <TextField
          sx={{ width: '100%' }}
          name='phone'
          type='text'
          label="Phone"
          value={name}
          onChange={(event) => {
            setName(event.target.value);
          }}
        />
        <TextField
          sx={{ width: '100%' }}
          name='companyName'
          type='text'
          label="Company Name"
          value={name}
          onChange={(event) => {
            setName(event.target.value);
          }}
        />
      </div>
      <div className='access_btn confirm_btn'>
        <button> Confirm </button>
      </div>
    </div>
  )
}

export default ConfirmPage