import { RouterProvider, createBrowserRouter } from 'react-router-dom';
import './App.css';
import { LoginPage, ConfirmPage } from './index'

function App() {
  const router = createBrowserRouter([
    {
      path: '/', element: <LoginPage />
    },
    {
      path: '/confirm', element: <ConfirmPage />
    },
  ]
  );

  return (
    <div className="app">
      <RouterProvider router={router} />
    </div>
  )
}

export default App
