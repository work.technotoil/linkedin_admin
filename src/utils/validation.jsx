import * as Yup from "yup";


export const confirmSchema = Yup.object().shape({
    name: Yup.string().required('Please enter your name'),
    country: Yup.string().required('Please enter your country'),
    email: Yup.string().email('Please enter valid email').required('Please enter your email'),
    phone: Yup.string().required('Please enter your phone number'),
    companyName: Yup.string().required('Please enter your company name'),
});