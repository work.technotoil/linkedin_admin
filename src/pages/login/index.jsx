import React from 'react'
import './index.css'
import LinkedInLogo from '../../assets/linkedin.png'
import { useNavigate } from 'react-router-dom'

const LoginPage = () => {
const navigate = useNavigate()


    const handleAccess = () =>{
        navigate('/confirm')
    }
    return (
        <div className='container_login'>
            <div className='linked_log'>
                <img src={LinkedInLogo} alt="" />
            </div>
            <div className='access_btn'>
                <button onClick={handleAccess}> Allow Access </button>
            </div>
        </div>
    )
}

export default LoginPage